<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoTiendaDAO.php";
class ProductoTienda{
    private $idProductoTienda;
    private $IdProducto;
    private $IdTienda;
    private $Cantidad;
    private $conexion;
    private $ProductoTiendaDAO;
    
   
    public function getIdProductoTienda()
    {
        return $this->idProductoTienda;
    }

    public function getIdProducto()
    {
        return $this->IdProducto;
    }

    public function getIdTienda()
    {
        return $this->IdTienda;
    }

    public function getCantidad()
    {
        return $this->Cantidad;
    }

    public function ProductoTienda($idProductoTienda="",$IdProducto="",$IdTienda="",$Cantidad=""){
        $this->idProductoTienda=$idProductoTienda;
        $this->IdProducto=$IdProducto;
        $this->IdTienda=$IdTienda;
        $this->Cantidad=$Cantidad;
        $this -> conexion = new conexion();
        $this -> ProductoTiendaDAO = new ProductoTiendaDAO($this->idProductoTienda,$this->IdProducto,$this->IdTienda,$this->Cantidad);
        
    }
    
    public function insertar(){
        $this -> conexion -> abrir();
        echo $this -> ProductoTiendaDAO -> insertar();
        $this -> conexion -> ejecutar($this -> ProductoTiendaDAO -> insertar());
        $this -> conexion -> cerrar();
    }

    
    public function consultarpdf(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoTiendaDAO -> consultarpdf());
        $Tiendas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new ProductoTienda("",$resultado[0], $resultado[1], $resultado[2]);
            array_push($Tiendas, $p);
        }
        $this -> conexion -> cerrar();
        return $Tiendas;
    }
    
    
}


?>