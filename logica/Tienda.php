<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/TiendaDAO.php";
class Tienda{
    private $id;
    private $nombre;
    private $direccion;
    private $conexion;
    private $TiendaDAO;
    

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getDireccion()
    {
        return $this->direccion;
    }

    public function Tienda($id="",$nombre="",$direccion=""){
        $this->id=$id;
        $this->nombre=$nombre;
        $this->direccion=$direccion;
        $this -> conexion = new conexion();
        $this -> TiendaDAO = new TiendaDAO($this->id,$this->nombre,$this->direccion);
        
    }
    
    public function insertar(){
        $this -> conexion -> abrir();
        echo $this -> TiendaDAO -> insertar();
        $this -> conexion -> ejecutar($this -> TiendaDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> TiendaDAO -> consultarTodos());
        $Tiendas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Tienda($resultado[0], $resultado[1], $resultado[2]);
            array_push($Tiendas, $p);
        }
        $this -> conexion -> cerrar();
        return $Tiendas;
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> TiendaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> id = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> direccion = $resultado[2];
    }
    

    
    
    
}


?>