<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoDAO.php";
class Producto{
    private $id;
    private $nombre;
    private $precio;
    private $conexion;
    private $productoDAO;
    
    
    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function Producto($id="",$nombre="",$precio=""){
        $this->id=$id;
        $this->nombre=$nombre;
        $this->precio=$precio;
        $this -> conexion = new conexion();
        $this -> productoDAO = new ProductoDAO($this->id,$this->nombre,$this->precio);
        
    }
    
    public function insertar(){
        $this -> conexion -> abrir();
        echo $this -> productoDAO -> insertar();
        $this -> conexion -> ejecutar($this -> productoDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> id = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> precio = $resultado[2];
    }
    
    
    
}


?>