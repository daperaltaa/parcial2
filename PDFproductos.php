<?php
require_once "logica/Cliente.php";
require_once "ezpdf/class.ezpdf.php";

$pdf = new Cezpdf("LETTER");
$pdf -> selectFont("ezpdf/fonts/Courier.afm");
$pdf -> ezSetCmMargins(2, 2, 3, 3);

$cliente = new Cliente();
$clientes = $cliente -> consultarTodos();

$pdf -> addJpegFromFile("img/portada.jpg", 3, 3, 10);

$opciones = array("justification" => "center");
$pdf -> ezText("<b>Tienda Virtual</b>", 20, $opciones);
$pdf -> ezText("<b>Reporte Clientes</b>", 16, $opciones);

//$encabezados = array("<b>#</b>","<b>Nombre</b>","<b>Apellido</b>","<b>Correo</b>","<b>Estado</b>");
$encabezados = array(
    "num" => "<b>#</b>",
    "nombre" => "<b>Nombre</b>",
    "apellido" => "<b>Apellido</b>",
    "correo" => "<b>Correo</b>",
    "estado" => "<b>estado</b>",
);
$datos = array();
$n = 5;
$i = 0;
while($n-- > 0){
    foreach ($clientes as $clientActual){
        $datos[$i]["num"] = $i + 1;
        $datos[$i]["nombre"] = $clientActual -> getNombre();
        $datos[$i]["apellido"] = $clientActual -> getApellido();
        $datos[$i]["correo"] = $clientActual -> getCorreo();
        $datos[$i]["estado"] = (($clientActual -> getEstado()==0)?"Deshabilitado":(($clientActual -> getEstado()==1)?"Habilitado":"Inactivo"));
        $i++;
    }    
}

$opcionesTabla = array(
    "showLines" => 2,
    "shaded" => 1,
    "shadeCol" => array(0.0, 0.8, 1),
    "rowGap" => 3
);
$pdf -> ezSetDY(-20);
$pdf -> ezTable($datos, $encabezados, "Lista de Clientes", $opcionesTabla);
$pdf -> ezNewPage();
$pdf -> ezTable($datos, $encabezados, "Lista de Clientes", $opcionesTabla);

$pdf -> ezStream();
// $pdfcode = $pdf->ezOutput();
// $fp=fopen("reportes/clientes.pdf",'wb');
// fwrite($fp,$pdfcode);
// fclose($fp);

?>