<?php
$nombre = "";
if (isset($_POST["nombre"])) {
    $nombre = $_POST["nombre"];
}


$Direccion = "";
if (isset($_POST["Direccion"])) {
    $Direccion = $_POST["Direccion"];
}

if (isset($_POST["crear"])) {
    $Tienda=new Tienda("",$nombre,$Direccion);
    $Tienda->insertar();

}
?>

<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Crear Tienda</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/Tienda/Registrar.php") ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
						</div>
						<div class="form-group">
							<label>Direccion</label> 
							<input type="text" name="Direccion" class="form-control" min="1" value="<?php echo $Direccion ?>" required>
						</div>
						
						<button type="submit" name="crear" class="btn btn-info">Crear <i class="fas fa-plus-circle"></i></button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>