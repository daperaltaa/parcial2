<?php
$Tienda=new Tienda();
$Tiendas = $Tienda -> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Producto</h4>
				</div>
				<div class="text-right"><?php echo count($Tiendas) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>id</th>
							<th>Nombre</th>
							<th>Precio</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($Tiendas as $TiendasActual){
						    echo "<tr>";
						    echo "<td>" . $TiendasActual -> getId() . "</td>";
						    echo "<td>" . $TiendasActual -> getNombre() . "</td>";
						    echo "<td>" . $TiendasActual -> getDireccion() . "</td>";
						    echo "<td><a href='index.php?pid=". base64_encode("presentacion/Tienda/Asignar.php") . "&idTienda=" . $TiendasActual -> getId() . "' data-toggle='tooltip' data-placement='left' title='Editar'><span class='fas fa-edit'></span></a></td>";			    
						    ///echo "<td><a href='PDFTiendas.php?idTienda=" echo $TiendasActual -> getId(). "' data-toggle='tooltip' data-placement='left' title='PDF'><span class='fas fa-file-pdf'></span></a></td>";
						    ?>
						    <td><a class="dropdown-item" href="PDFTiendas.php?idTienda=<?php echo$TiendasActual ->getId()?>" target="_blank"><i class="fas fa-file-pdf"></i></a></td>
						    <?php 
						    echo "</tr>";
						    $i++;
						    
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>