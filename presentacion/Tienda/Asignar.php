<?php
$IdProducto=0;

if(isset($_POST["editar"])){
   
    $producoTienda=new ProductoTienda("",$_POST["Producto"],$_GET["idTienda"],$_POST["Cantidad"]);
    $producoTienda->insertar();
    $Tienda = new Tienda($_GET["idTienda"]);
    $Tienda -> consultar();
    $producto = new Producto();
    $Productos=$producto->consultarTodos();


}else{
    $Tienda = new Tienda($_GET["idTienda"]);
    $Tienda -> consultar();  
    $producto = new Producto();
    $Productos=$producto->consultarTodos();
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Editar Producto</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["editar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/Tienda/Asignar.php") ?>&idTienda=<?php echo $_GET["idTienda"]?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Tienda</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $Tienda -> getNombre() ?>" required>
						</div>

							<div class="form-group col-md-8">
							<label for="inputState">Producto</label> 
							<select class="form-control" name="Producto">
								<?php 
								foreach($Productos as $productoActual){
							
								    echo "<option value=".$productoActual->getId().">".$productoActual->getNombre()."</option>";
								 }?>
							
							</select>
						</div>	

						<div class="form-group">
							<label>Cantidad</label> 
							<input type="number" name="Cantidad" class="form-control" min="1" value="<?php echo "" ?>" required>
						</div>
						<button type="submit" name="editar" class="btn btn-info">Editar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>