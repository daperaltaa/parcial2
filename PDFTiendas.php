<?php
require_once "fpdf/fpdf.php";
require_once "logica/ProductoTienda.php";

$pdf = new FPDF("P", "mm", "Letter");
$pdf -> SetFont("Courier", "B", 20);
$pdf -> AddPage();
$pdf ->SetXY(0, 0);
$pdf -> Cell(216, 20, "Tienda Virtual", 0, 2, "C");
$pdf -> Cell(216, 15, "Reporte TIENDA", 0, 2, "C");

$ProductoTienda=new ProductoTienda("","",$_GET["idTienda"]);

$pdf ->Ln();
$pdf ->Cell(50,10,"id-Tienda",1);
$pdf ->Cell(50,10,"id-Producto",1);
$pdf ->Cell(70,10,"Cantidad",1);
$pdf ->Ln();
$i=1;
$ProductoTiendas=$ProductoTienda->consultarpdf();

foreach ($ProductoTiendas as $articuloActual){

    $pdf ->Cell(50,10,$articuloActual -> getIdTienda(),1);
    $pdf ->Cell(50,10,$articuloActual-> getIdProducto(),1);
    $pdf ->Cell(70,10,$articuloActual -> getCantidad(),1);
    $pdf ->Ln();
    $i++;
}

$pdf -> Output();


?>